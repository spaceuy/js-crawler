import * as puppeteer from 'puppeteer';
import { Category } from './category/category.interface';
import { getCategoriesFromDirectory } from './directory/directory.crawler';
import { getEntrantsFromCategory } from './category/category.crawler';
import { Profile } from './profile/profile.interface';
import { getProfileData } from './profile/profile.crawler';

(async () => {
  console.time()
  // const categories = await getCategoriesFromDirectory('2020');
  const profileLinks = await getEntrantsFromCategory('2020', 'games');
  // console.log(JSON.stringify(categories));

  // USE THIS SECTION TO GET ARRAY OF PROFILES
  // const profiles: Profile[] = [];
  // for (const link of profileLinks) {
  //   profiles.push(await getProfileData(link, '2020', 'games'));
  // }

  console.log(JSON.stringify(await getProfileData(profileLinks[0], '2020', 'games')));
  console.timeEnd();
})()