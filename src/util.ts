export async function getInnerTextFromEval(element: any, selector: string): Promise<string> {
  return element.$eval(selector, (node: any) => node.innerText);
}