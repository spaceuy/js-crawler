export interface Profile {
  id: string;
  link: string;
  name: string;
  imgUrl: string;
  year: string;
  category: string;
  type: 'group' | 'person';
  description: string;
  role: string;
  group?: string;
  age?: number;
  residence?: string;
  education?: string;
}