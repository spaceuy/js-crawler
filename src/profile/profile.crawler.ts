import { Profile } from "./profile.interface";
import getPage from "../page";
import { Page, Browser } from "puppeteer";
import { getInnerTextFromEval } from "../util";

async function isGroup(page: Page): Promise<boolean> {
    const states = await page.$('div.profile-stats');
    return !states;
}

export async function getProfileData(link: string, year: string, category: string): Promise<Profile> {
  const [browser, page] = await getPage(link);
  const id = link.substr(0, link.length - 1).split('/').pop() as string;
  const type = (await isGroup(page)) ? 'group' : 'person';
  const name = await getInnerTextFromEval(page, 'h1');
  const imgUrl = await getInnerTextFromEval(page, 'img.profile-photo__img');
  const description = await getInnerTextFromEval(page, 'div.profile-text > p');
  const roleAndGroup = await getInnerTextFromEval(page, 'div.profile-subheading');
  const profile: Profile = {
    id,
    type,
    name,
    imgUrl,
    year,
    category,
    link,
    description,
    role: roleAndGroup.includes(',')
      ? roleAndGroup.split(',')[0].trim()
      : roleAndGroup,
    group: roleAndGroup.includes(',')
      ? roleAndGroup.split(',')[1].trim()
      : undefined,
  };

  if (type === 'person') {
    const stats = (await page.$x('//div[@class="profile-stats__item"]/div'));
    stats.shift(); //remove stats header;
    try {
      profile.age = +await getInnerTextFromEval(stats[0], 'span.profile-stats__text');
      profile.residence = await getInnerTextFromEval(stats[1], 'span.profile-stats__text');
      profile.education = await getInnerTextFromEval(stats[2], 'span.profile-stats__text');
    } catch (e) {}
  }
  await browser.close();
  return profile;
}