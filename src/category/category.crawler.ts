import { Profile } from "../profile/profile.interface";
import getPage from "../page";
import { Browser } from "puppeteer";

export async function getEntrantsFromCategory(year: string, category: string): Promise<string[]> {
  const [browser, page] = await getPage(`https://www.forbes.com/30-under-30/${year}/${category}`);
  const entrants = await page.$$('.card-front');
  const modalOverlay = await page.$('div.modal-overlay')
  const entrantLinks: string[] = [];

  const firstEntrant = entrants.shift();
  await firstEntrant?.click();
  await page.waitFor(1000);
  const firstLink = await page.$eval('a.full-profile', (node: any) => node.href) as unknown as string;
  entrantLinks.push(firstLink.split('?')[0]);

  for (const entrant of entrants) {
    const nextButton = await page.$('button.slick-next');
    await nextButton?.click();
    // await entrant.click();
    await page.waitFor(500);
    const link = await page.$eval('a.full-profile', (node: any) => node.href) as unknown as string;
    entrantLinks.push(link.split('?')[0]);
    // await modalOverlay?.click();
  }
  await browser.close()
  return entrantLinks;
}