import getPage from "../page";
import { Category } from "../category/category.interface";

export async function getCategoriesFromDirectory(year: string): Promise<Category[]> {
  const [browser, page] = await getPage(`https://www.forbes.com/30-under-30/${year}`);
  const categoryInfoDivs = await page.$x('//a[@class="cat-link"]/..');
  const categories: Category[] = [];
  for (const categoryInfoDiv of categoryInfoDivs) {
    categories.push({
      id: await categoryInfoDiv.$eval('a', (node: any) => node.href.substring(0, node.href.length - 1).split('/').pop()) as unknown as string,
      name: await categoryInfoDiv.$eval('span', (node: any) => node.innerText) as unknown as string,
      link: await categoryInfoDiv.$eval('a', (node: any) => node.href) as unknown as string,
    });
  }
  return categories;
}