import { Page, launch, Browser } from "puppeteer";

export default async function getPage(url: string): Promise<[Browser, Page]> {
  const browser = await launch();
  const page = await browser.newPage();
  await page.goto(url, { timeout: 0, waitUntil: 'networkidle2' });
  return [browser, page];
}